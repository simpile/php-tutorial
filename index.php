<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Syntax</title>
</head>
<h1>php syntax</h1>
<hr>
<h2>Basic PHP Syntax</h2>
<ul>
<li>A PHP script can be placed anywhere in the document.</li>
<li>There are four different pairs of opening and closing tags which can be used in php</li>
    <ol>
    
        <li>Defult syntax, which starts with <b>&lt;?php</b> and ends with <b>?&gt;</b></li>
        <?php
        echo 'if you want to serve PHP code in XHTML or XML documents,
                use these tags'; 
        //  DEFULT SYNTAX FOR PHP ?>
        <li>Short echo tag, which starts with <b>&lt;=</b> and ends with <b>?&gt;</b></li>
        
        <?= 'print this string'; /*SHORT ECHO TAG FOR PHP 
        THIS IS A MULTI LINE COMMENT*/?>
        <li>Short Tags or Short open Tags, which starts with <b>&lt;</b> and ends with <b>?&gt;</b></li>
        <? echo 'this code is within short tags, but will only work '.
            'if short_open_tag is enabled'; #SHORT TAG FOR PHP, THIS ALSO IS AN ONE LINE COMMENT ?>
        <li>If a file contains only PHP code, it is preferable to omit the PHP closing tag at the end of the file.</li>
  
        <?php
        echo "Hello world";

        // ... more code

        echo "Last statement";

       // the script ends here with no PHP closing tag
       
    </ol>
<li>PHP requires instructions to be terminated with a semicolon at the end of each statement. </li>
<?php echo "Some text"; ?>
No newline
<?= "But newline now" ?>
<li>closing tag of a block of PHP code automatically implies a semicolon;</li>
<section>
<h4>NOTE:</h4>
<p>The closing tag of a PHP block at the end of a file is optional</p>

<?php
    echo 'This is a test';
?>

<?php echo 'This is a test' ?>

<?php echo 'We omitted the last closing tag';

</section>
</ul>
<body>
    
</body>
</html>